﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace milGearsPrototype.Writers
{
    public class milGearsFinalReportWriter : ImilGearsWriter

    {
        public void Write(milGearsRecord record, string filePath)
        {
            Document doc = new Document();
            //FileStream stream = new FileStream("SimplePDF.pdf", FileMode.Create);
            FileStream stream = new FileStream(filePath, FileMode.Create);
            PdfWriter pdf = PdfWriter.GetInstance(doc, stream);

            doc.Open();
            doc.Add(new Paragraph("Name: " + record.FirstName ));
            doc.Add(new Paragraph("Rank: " + record.Rank ));
            doc.Add(new Paragraph("Serial Number: " + record.SerialNumber ));            
            doc.Add(new Paragraph("This PDF was generated at " + DateTime.Now.ToLongTimeString()));
            doc.Close();
        }
    }
}