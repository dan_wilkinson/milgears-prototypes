﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milGearsPrototype.Writers
{
    public interface ImilGearsWriter
    {
        void Write(milGearsRecord record, string filePath);
        //byte[] Write(milGearsRecord record);
    }
}
