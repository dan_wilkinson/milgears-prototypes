﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Xml.Serialization;

namespace milGearsPrototype.Writers
{
    public class milGearsSessionWriter : ImilGearsWriter
    {
        public void Write(milGearsRecord record, string filePath)
        {
            var x = new XmlSerializer(typeof(milGearsRecord));
            var stream = new StreamWriter(filePath);
            x.Serialize(stream, record);

            stream.Close();

            File.Copy(filePath, filePath + ".plaintext", true);
            File.Encrypt(filePath);

        }

        //public byte[] Write(milGearsRecord record)
        //{
        //    var x = new XmlSerializer(typeof(milGearsRecord));
        //    var stream = new StreamWriter(filePath);
        //    x.Serialize(stream, record);

        //    stream.Close();

        //    File.Copy(filePath, filePath + ".plaintext", true);
        //    File.Encrypt(filePath);


        //}
    }
}