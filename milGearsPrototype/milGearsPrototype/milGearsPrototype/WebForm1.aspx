﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="milGearsPrototype.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td> Welcome, <asp:Label ID="lblUserName" runat="server" /></td>
                    <td>Please enter your information.</td>
                </tr>
                <tr>
                    <td>(Optional) Import prior session</td>
                    <td><asp:Button ID="btnImportSession" runat="server" Text="Import Session" OnClick="btnImportSession_Click" /></td>
                </tr>
                <tr>
                    <td>(Optional) Import PDF data</td>
                    <td><asp:FileUpload ID="filePDF" runat="server" /><asp:Button ID="btnImportPDF" runat="server" Text="Import" OnClick="btnImportPDF_Click" /></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><asp:TextBox ID="txtFirstName" runat="server" /></td>
                </tr>
                <tr>
                    <td>Rank</td>
                    <td><asp:DropDownList ID="ddlRank" runat="server" >
                        <asp:ListItem Text="Corporal" />
                        <asp:ListItem Text="Seargent" />
                        <asp:ListItem Text="Captain" />
                        <asp:ListItem Text="Major" />
                        <asp:ListItem Text ="General" />
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Serial Number</td>
                    <td><asp:TextBox ID="txtSerialNumber" runat="server" /></td>
                </tr>
                <tr>
                    <td><asp:Button ID="btnSave" runat="server" Text="Save and complete later" OnClick="btnSave_Click" /></td>
                    <td><asp:Button ID="btnSubmit" runat="server" Text="Generate PDF" OnClick="btnSubmit_Click" /></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblConfirm" runat="server" ForeColor="Red" /></td>
                    <td><asp:HyperLink ID="linkFinalReport" runat="server" Visible="false" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
