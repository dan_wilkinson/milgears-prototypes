﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using milGearsPrototype.Readers;
using milGearsPrototype.Writers;

namespace milGearsPrototype
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var relativePath = "/GeneratedDocs/ABC.PDF";
            var filePath = Server.MapPath(relativePath);
            milGearsFinalReportWriter writer = new milGearsFinalReportWriter();
            var record = getRecordFromForm();
            writer.Write(record, filePath);
            linkFinalReport.Visible = true;
            linkFinalReport.Text = relativePath;
            linkFinalReport.NavigateUrl = relativePath;

            lblConfirm.Text = "You can view the generated PDF file here.";


            //var bytes = File.ReadAllBytes(filePath);
            ////var reader = new BinaryReader ()

            //Response.ClearContent();
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "inline; filename=ABC.PDF");
            //Response.AddHeader("Content-Length", bytes.Length.ToString ());
            //Response.BinaryWrite(bytes);
            //Response.End();


        }

        milGearsRecord getRecordFromForm()
        {
            milGearsRecord record = new milGearsRecord ();
            record.FirstName = txtFirstName.Text;
            record.Rank = ddlRank.Items[ddlRank.SelectedIndex].Text;
            record.SerialNumber = txtSerialNumber.Text;
            return record;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var relativePath = "/GeneratedDocs/Session.milGears";
            var filePath = Server.MapPath(relativePath);
            var writer = new milGearsSessionWriter();
            var record = getRecordFromForm();
            writer.Write(record, filePath);
            linkFinalReport.Visible = false;
            lblConfirm.Text = "Your session has been saved.";


            //var bytes = File.ReadAllBytes(filePath);
            ////var reader = new BinaryReader ()

            ////Response.ClearContent();
            //////Response.ContentType = "application/pdf";
            ////Response.ContentType = "application/x-binary";            
            //////Response.AddHeader("Content-Disposition", "inline; filename=ABC.PDF");
            //////Response.AddHeader("Content-Disposition", "inline; filename=Session.milGears");
            ////Response.AddHeader("Content-Disposition", "attachment; filename=Session.milGears");
            ////Response.AddHeader("Content-Length", bytes.Length.ToString());
            ////Response.BinaryWrite(bytes);
            ////Response.End();

            //Response.ClearContent();
            //Response.ContentType = "application/x-binary";
            //Response.AddHeader("Content-Disposition", "attachment; filename=Session.milGears");
            //Response.TransmitFile(filePath);
            //Response.End();

        }

        protected void btnImportSession_Click(object sender, EventArgs e)
        {
            var relativePath = "/GeneratedDocs/Session.milGears";
            var filePath = Server.MapPath(relativePath);
            var record = new milGearsSessionReader().Read(filePath);
            PopulateFormControls(record);
            lblConfirm.Text = "Your session has been reloaded.";

        }


        private void PopulateFormControls(milGearsRecord record)
        {
            txtFirstName.Text = record.FirstName;
            ddlRank.SelectedIndex = ddlRank.Items.IndexOf(ddlRank.Items.FindByText(record.Rank));
            txtSerialNumber.Text = record.SerialNumber;
        }

        protected void btnImportPDF_Click(object sender, EventArgs e)
        {
            //var relativePath = "/GeneratedDocs/Session.milGears";
            //var filePath = Server.MapPath(relativePath);            
            //var filePath = filePDF.FileName;
            var record = new milGearsPDFReader().Read(filePDF.FileBytes);
            PopulateFormControls(record);
            lblConfirm.Text = "Your data has been imported.";
        }
    }
}