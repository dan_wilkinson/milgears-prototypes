﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milGearsPrototype.Readers
{
    public interface ImilGearsReader
    {
        milGearsRecord Read(string filePath);
    }
}
