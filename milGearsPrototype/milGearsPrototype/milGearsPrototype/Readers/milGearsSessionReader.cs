﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace milGearsPrototype.Readers
{
    public class milGearsSessionReader : ImilGearsReader
    {
        public milGearsRecord Read(string filePath)
        {
            milGearsRecord retval = null;
            var stream = new StreamReader(filePath);
            var serializer = new XmlSerializer(typeof (milGearsRecord));

            retval = serializer.Deserialize(stream) as milGearsRecord;

            stream.Close();
            return retval;
        }
    }
}