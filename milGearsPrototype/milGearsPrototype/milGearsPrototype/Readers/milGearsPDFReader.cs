﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace milGearsPrototype.Readers
{
    public class milGearsPDFReader : ImilGearsReader
    {
        public milGearsRecord Read(string filePath)
        {
            milGearsRecord retval = null;
            var reader = new PdfReader(filePath);
            var fields = reader.AcroFields.Fields;
            return retval;
        }

        public milGearsRecord Read(byte[] bytes)
        {
            var reader = new PdfReader(bytes);
            var retval = new milGearsRecord();
            var fields = reader.AcroFields.Fields;

            foreach (var field in fields)
            {
                var tmp = string.Format("{0}: {1}", field.Key, reader.AcroFields.GetField(field.Key));
                System.Diagnostics.Debug.WriteLine(tmp);
            }

            retval.FirstName = reader.AcroFields.GetField("Name");
            retval.Rank = reader.AcroFields.GetField("BranchStatus");

            return retval;
        }
    }
}