﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace iTextSharpTemplatePDFDemo
{
    public partial class Form1 : Form
    {
        // Temporary field values to test populating the PDF
        string docTitle = "Generated PDF Test";
        string firstName = "Joe";
        string lastName = "Smith";
        string age = "21";
        string email = "joe.smith@navy.mil";
        string location = "Great Lakes, IL 60044";
        string branchStatus = "Navy / Active Duty";
        string ratePayGrade = "Engineman / E4";
        string datesOfRank = "25 Jan 2017 – Present";

        public Form1()
        {
            InitializeComponent();
        }

        private void Generate_Click(object sender, EventArgs e)
        {
            fillPDF();
        }

        public void fillPDF()
        {
            string fileNameExisting = Application.StartupPath + @"\Template.pdf";
            string fileNameNew = Application.StartupPath + @"\new.pdf";
            iTextSharp.text.Image checkImage = iTextSharp.text.Image.GetInstance(Application.StartupPath + @"\check.png");

            //Console.WriteLine("Template Path: " + fileNameExisting);

            using (var existingFileStream = new FileStream(fileNameExisting, FileMode.Open))
            using (var newFileStream = new FileStream(fileNameNew, FileMode.Create))
            {
                // Open existing PDF
                var pdfReader = new PdfReader(existingFileStream);

                // PdfStamper, which will create the document
                var stamper = new PdfStamper(pdfReader, newFileStream);

                var form = stamper.AcroFields;
                var fieldKeys = form.Fields.Keys;                

                foreach (string fieldKey in fieldKeys)
                {
                    //Console.WriteLine("fieldKey: " + fieldKey);

                    #region Field Key Names
                    /*
                     *  fieldKey: DocumentTitle
                        fieldKey: Name
                        fieldKey: Age
                        fieldKey: EMail
                        fieldKey: CityStateZip
                        fieldKey: BranchStatus
                        fieldKey: RatePayGrade
                        fieldKey: DatesOfRank
                        fieldKey: HoldUSCGMMC
                        fieldKey: SubmitProofOfMMC
                        fieldKey: SubmitApplicationForTSA
                        fieldKey: SubmitProofOfDrugScreening
                        fieldKey: SubmitApplicationForMedicalCert
                        fieldKey: AccumulatedSeaService
                        fieldKey: OtherMilitaryServiceOrSeaActivity
                        fieldKey: HasSeaServiceLetter
                        fieldKey: SeaServiceVessel
                        fieldKey: SeaServicePeriod
                        fieldKey: SeaServiceBilletRank
                        fieldKey: SeaServiceWatches
                        fieldKey: SeaServiceDaysUW
                        fieldKey: SeaServiceHP
                        fieldKey: SeaServiceDaysInPort
                        fieldKey: SeaServiceAO
                        fieldKey: SeaServiceProp
                        fieldKey: SeaServiceTonnage
                        fieldKey: CreditableSeaTime
                        fieldKey: CalculatedSeaService
                        fieldKey: NavyRequiredTraining
                        fieldKey: MarinerPQSTraining
                        fieldKey: CertificationsLicensesApprenticeships
                        fieldKey: MMCGeneralRequirement1
                        fieldKey: MMCGeneralRequirement2
                        fieldKey: MMCGeneralRequirement3
                        fieldKey: MMCGeneralRequirement4a
                        fieldKey: MMCGeneralRequirement4b
                        fieldKey: MMCGeneralRequirement4c
                        fieldKey: MMCGeneralRequirement4d
                        fieldKey: MMCGeneralRequirement5
                     */
                    #endregion
                    switch (fieldKey)
                    {
                        case "DocumentTitle":
                            form.SetField(fieldKey, docTitle);
                            break;
                        case "Name":
                            form.SetField(fieldKey, firstName + " " + lastName);
                            break;
                        case "Age":
                            form.SetField(fieldKey, age);
                            break;
                        case "EMail":
                            form.SetField(fieldKey, email);
                            break;
                        case "CityStateZip":
                            form.SetField(fieldKey, location);
                            break;
                        case "BranchStatus":
                            form.SetField(fieldKey, branchStatus);
                            break;
                        case "HoldUSCGMMC":
                            // Populate a checkmark image into a form field area instead of text
                            PdfContentByte contentByte = stamper.GetOverContent(1); // This is only looking at page 1

                            // Image positioning
                            IList<iTextSharp.text.pdf.AcroFields.FieldPosition> imagePosition = null;
                            imagePosition = form.GetFieldPositions(fieldKey);
                            float left = imagePosition[0].position.GetLeft(0);
                            float right = imagePosition[0].position.GetRight(0);
                            float top = imagePosition[0].position.GetTop(0);
                            float bottom = imagePosition[0].position.GetBottom(0);

                            // Scale image to field area
                            checkImage.ScaleToFit(right - left, top - bottom);

                            // Set the image position
                            checkImage.SetAbsolutePosition(left + ((right - left) - checkImage.ScaledWidth) /
    2, top - checkImage.ScaledHeight);

                            // Add the image
                            contentByte.AddImage(checkImage);
                            break;
                    }                    
                }

                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;

                stamper.Close();
                pdfReader.Close();
            }
        }
    }
}
