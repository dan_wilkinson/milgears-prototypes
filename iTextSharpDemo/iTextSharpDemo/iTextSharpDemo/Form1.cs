﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace iTextSharpDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Document doc = new Document();            
            FileStream stream = new FileStream("SimplePDF.pdf", FileMode.Create);
            PdfWriter pdf = PdfWriter.GetInstance(doc, stream);

            doc.Open();
            doc.Add(new Paragraph("This PDF was generated at " + DateTime.Now.ToLongTimeString()));
            doc.Close();
            

        }
    }
}
